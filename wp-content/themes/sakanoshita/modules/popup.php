<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<?php wp_head();?>
</head>

<body leftmargin="0" topmargin="0">
<div class="products">
<center>
  <table border="0" cellspacing="0" cellpadding="0"  style="background-color: #fff;">
	<tr>
	  <td>&nbsp;</td>
	</tr>
	<tr>
	  <td bgcolor="#FFFFFF"><img src="<?php echo get_stylesheet_directory_uri(); ?>/product/pict/spacer.gif" width="8" height="20"></td>
	</tr>
	<tr>
	  <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td width="30"><img src="<?php echo get_stylesheet_directory_uri(); ?>/product/pict/spacer.gif" width="30" height="20"></td>
			<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				  <td bgcolor="#E1FFD2"><table width="100%" border="0" cellspacing="0" cellpadding="3">
					  <tr>
						  <td><h4 class="ttlLev2"><?php the_title();?></h4></td>
					  </tr>
					</table></td>
				</tr>
				<tr>
				  <td><img src="<?php echo get_stylesheet_directory_uri(); ?>/product/pict/spacer.gif" width="8" height="25"></td>
				</tr>
				<tr>
				  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td width="17"><img src="<?php echo get_stylesheet_directory_uri(); ?>/product/pict/spacer.gif" width="17" height="10"></td>
						<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
							<?php
								if( have_rows('product_images') ):
								while ( have_rows('product_images') ) : the_row();
									$product_image_id = get_sub_field('product_image');
									$product_image_src = wp_get_attachment_image_src($product_image_id , 'full');
							?>
								<tr>
									<td class="text12">
									<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr>
									<td><h5><?php echo get_sub_field('product_ttl');?></h5></td>
									</tr>
									<tr>
									<td align="center">
										<img src="<?php echo $product_image_src[0]; ?>" alt="<?php echo get_sub_field('product_ttl');?>">
									</td>
									</tr>
									</table>
									</td>
								</tr>
							<?php endwhile;endif;?>

						  </table></td>
						<td width="17" align="right"><img src="<?php echo get_stylesheet_directory_uri(); ?>/product/pict/spacer.gif" width="17" height="10"></td>
					  </tr>
					</table></td>
				</tr>
				<tr>
				  <td><img src="<?php echo get_stylesheet_directory_uri(); ?>/product/pict/spacer.gif" width="8" height="15"></td>
				</tr>
				<tr>
				  <td align="center"><Form>
					  <Input name="button" type="button" onClick="javascript:window.close();" value="閉じる">
					</Form></td>
				</tr>
			  </table></td>
			<td width="30" align="right"><img src="<?php echo get_stylesheet_directory_uri(); ?>/product/pict/spacer.gif" width="30" height="20"></td>
		  </tr>
		</table></td>
	</tr>
	<tr>
	  <td class="p_footer"><p class="copyright">&#169; 2017 SAKANOSHITA CO., LTD.</p></td>
	</tr>
	<tr>
	  <td height="20" align="left" class="font12"><img src="<?php echo get_stylesheet_directory_uri(); ?>/product/pict/spacer.gif" width="8" height="20">
	  </td>
	</tr>
  </table>
</center>
</div>

<?php wp_footer();?>
</body>
</html>
