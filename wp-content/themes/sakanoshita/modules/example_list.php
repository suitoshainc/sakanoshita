<?php $terms = get_terms('example_category','orderby=order&hide_empty=0');?>

<div class="example">
	<h2 class="exampleTtl"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/example_ttl.png" width="304" height="57" alt="FA機器導入事例"></h2>
	<?php
	if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('
	<p class="pagePath clearfix">','</p>
	');
	}
	?>


	<div class="outlineBox">
		<h3 class="ttlLev1">FA機器導入事例</h3>
		<dl class="dlStyle02">
		<dt>高次元のシステム提案を通じて</dt>
		<dd>サカノシタのFA事業は、自動機製造メーカーと機械構成部品メーカーとの相互協力により、<br>
		より高度なシステムを構築すべく活動をしております。協力会社も、<br>
		大型設備から顕微鏡レベルの極小部品まで取り扱う企業を選りすぐっております。</dd>
		</dl>
	</div>
	<?php
	foreach($terms as $term):
		$flg = $term->taxonomy.'_'.$term->term_id;
		$fa_catecory_title = get_field('fa_catecory_title', $flg);
	?>
		<div class="outlineBox">
			<h4 class="ttlLev2"><?php echo $term->name;?></h4>
			<div class="innerBox">
				<h5 class="ttlLev3"><?php echo $fa_catecory_title;?></h5>
				<div class="example2Col clearfix mb1em">
				<div class="photo">
					<?php
						if( have_rows('fa_catecory_images', $flg) ):
						while ( have_rows('fa_catecory_images', $flg) ) : the_row();

						$fa_catecory_image_id = get_sub_field('fa_catecory_image');
						$fa_catecory_image_src = wp_get_attachment_image_src($fa_catecory_image_id , 'full');
					?>
						<img src="<?php echo $fa_catecory_image_src[0];?>" width="200">
						<p class="caption"><?php echo get_sub_field('fa_catecory_caption');?></p>
					<?php endwhile;endif;?>
				</div>

				<?php
					$myQuery = new WP_Query();
					$param = array(
						'posts_per_page' => -1,
						'post_type' => 'example',
						'tax_query' =>array(
							array(
								'taxonomy' => $term->taxonomy,
								'terms'=> $term->slug,
								'field'=> 'slug'
							)
						)
					);
					$myQuery->query($param);
				?>
				<?php if($myQuery->have_posts()): while($myQuery->have_posts()) : $myQuery->the_post(); ?>
					<dl class="txt">
						<dt><?php the_title();?></dt>
						<dd>
							<ul>
								<?php
									if( have_rows('fa_list') ):
									while ( have_rows('fa_list') ) : the_row();
								?>
									<li>・<?php echo get_sub_field('fa_list_name');?></li>
								<?php endwhile;endif;?>
							</ul>
						</dd>
					</dl>
				<?php endwhile;endif;wp_reset_postdata();?>
				</div>
			</div><!-- innerBox out -->
		</div>
	<?php endforeach;?>
</div>
