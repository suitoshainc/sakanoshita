<?php
$post_type_obj = get_post_type_object($post_type);
$col = 5;

$taxonomy = array_values(array_unique($type_and_tax->get_tax($post_type)));
$terms = get_terms($taxonomy[0],'orderby=order&hide_empty=0');


if( have_rows('category_data','Options') ){
	while ( have_rows('category_data','Options') ){
		the_row();
		$target = get_sub_field('cat_target');

		if($taxonomy[0] == $target){
			$col = get_sub_field('cat_list');
			break;
		}
	}
}

$terms_chunked = array_chunk($terms, $col);
$class = 'goodsList'.$col.'Col';
?>

<script>
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
(function($) {
$(function(){
	$('.breadcrumb_last').before('<a href="<?php echo home_url('products');?>" rel="v:url" property="v:title">取扱商品</a>');
});
})(jQuery);
</script>

<div class="products">
	<h2 class="productsTtl"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/products_ttl.png" width="186" height="57" alt="取扱商品"></h2>
	<?php
	if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('
	<p class="pagePath clearfix">','</p>
	');
	}
	?>

	<div class="outlineBox">
	<h3 class="ttlLev1"><?php echo $post_type_obj->label;?></h3>
	<div class="innerBox">

	<div class="products2Col clearfix mb1em">
	<?php if($terms):?>
		<?php foreach($terms_chunked as $terms_c):?>
			<ul class="<?php echo $class;?> clearfix">
				<?php foreach($terms_c as $term_c):?>
					<li><a href="#<?php echo $term_c->slug;?>"><?php echo $term_c->name;?></a></li>
				<?php endforeach;?>
			</ul>
		<?php endforeach;?>
	<?php endif;?>

	<h5 class="ttlLev3">下線部付の商品名をクリックすると商品写真がご覧いただけます。</h5>
	</div><!-- innerBox out -->
	</div><!-- outlineBox out -->

	<?php foreach($terms as $term):?>
	<div id="<?php echo $term->slug;?>" class="outlineBox">
		<h4 class="ttlLev2"><?php echo $term->name?></h4>
		<div class="innerBox">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableStyle_p">
			<tbody>
				<?php
					$myQuery = new WP_Query();
					$param = array(
						'posts_per_page' => -1,
						'post_type' => $post_type,
						'tax_query' =>array(
							array(
								'taxonomy' => $taxonomy[0],
								'terms'=> $term->slug,
								'field'=> 'slug'
							)
						)
					);
					$myQuery->query($param);
				?>
				<?php if($myQuery->have_posts()): while($myQuery->have_posts()) : $myQuery->the_post(); ?>
					<tr>
						<th><?php the_title();?></th>
						<td>
							<?php if(get_field('product_images')):?>
								<a href="javascript:;" onclick="MM_openBrWindow('<?php the_permalink();?>','<?php the_title();?>','menubar=yes,scrollbars=yes,resizable=yes,width=550,height=600')"><?php echo get_field('product_content');?></a>
							<?php else:?>
								<?php echo get_field('product_content');?>
							<?php endif;?>

						</td>
					</tr>
				<?php endwhile;endif;wp_reset_postdata();?>
			</tbody>
			</table>
		</div><!-- innerBox out -->
	</div><!-- outlineBox out -->
	<?php endforeach;?>
	</div>
</div>
