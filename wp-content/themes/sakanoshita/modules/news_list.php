<h2 class="newsTtl"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/news_ttl.png" width="207" height="58" alt="SAKANOSHITA NEWS"></h2>

<div class="outlineBox">
<h3 class="ttlLev1">お知らせ</h3>
	<?php
		if(get_field('info_lower_num','Options')){
			$num = get_field('info_lower_num','Options');
		}else{
			$num = -1;
		}
		$myQuery = new WP_Query();
		$param = array(
			'post_type' => 'news',
			'posts_per_page' => $num,
			'paged' => $paged
		);
		$myQuery->query($param);
	?>
	<?php if($myQuery->have_posts()): while($myQuery->have_posts()) : $myQuery->the_post(); ?>
	<dl class="dlStyle01">
		<dt><?php the_title();?></dt>
		<dd>
			<time><?php the_time('Y年m月d日')?></time>
			<p>
			<?php
				$link = '';
				if(get_field('news_file')){
					$link = get_field('news_file');
				}

				if(get_field('news_link')){
					$link = get_field('news_link');
				}

				$text = get_field('news_link_test');

				if($link):
			?>
					<a href="<?php echo $link;?>"><?php echo $text;?></a>
				<?php else:?>
					<?php echo $text;?>
				<?php endif;?>
			</p>
		</dd>
	</dl>
	<?php endwhile;endif;if(function_exists('wp_pagenavi')) wp_pagenavi(array('query' => $myQuery ));wp_reset_postdata();?>
</div>
