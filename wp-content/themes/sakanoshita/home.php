<?php get_header(); ?>

<?php
if( have_rows('top_slide','Options') ):
?>
<div class="mainVisual">
	<ul class="bxslider">
	<?php
		while ( have_rows('top_slide','Options') ) : the_row();
		$top_slide_img_id = get_sub_field('top_slide_img');
		$slide_img_src = wp_get_attachment_image_src($top_slide_img_id , 'full');
		$top_img_link = get_sub_field('top_img_link');
	?>
	<li>
	<?php if($top_img_link) echo '<a href="'.$top_img_link.'">';?>
		<img src="<?php echo $slide_img_src[0];?>" alt="<?php the_sub_field('top_img_alt');?>">
	<?php if($top_img_link) echo '</a>';?>
	</li>
	<?php endwhile;?>
	</ul>
</div>
<script>
jQuery.noConflict();
(function($) {
$(document).ready(function() {
	$('.bxslider').bxSlider({
		//mode: 'fade',
		controls: true,
		pager: false,
		auto: true
	});
});
})(jQuery);
</script>
<?php endif;?>
<div class="newsList">
	<h2 class="newsTtl">SAKANOSHITA NEWS　　　　　　　　　　　　　　　　　　　　　　　　　  <a href="<?php echo home_url('news');?>">【お知らせ一覧】</a>
</h2>
	<ul>
		<?php
			if(get_field('info_num','Options')){
				$num = get_field('info_num','Options');
			}else{
				$num = -1;
			}
			$myQuery = new WP_Query();
			$param = array(
				'post_type' => 'news',
				'posts_per_page' => $num
			);
			$myQuery->query($param);
		?>
		<?php if($myQuery->have_posts()): while($myQuery->have_posts()) : $myQuery->the_post(); ?>
			<li>
				<time><?php the_time('Y年m月d日');?></time>
				<div class="text">
					<?php if(get_field('news_check') > 0):?>
					<a href="<?php echo home_url('news')?>#post<?php echo $post->ID;?>">
						<?php the_title();?>
					</a>
					<?php endif;?>

					<?php
						$link = '';
						if(get_field('news_file')){
							$link = get_field('news_file');
						}

						if(get_field('news_link')){
							$link = get_field('news_link');
						}

						$text = get_field('news_link_test');

						if($link):
					?>
						<a href="<?php echo $link;?>"><?php echo $text;?></a>
					<?php else:?>
						<?php echo $text;?>
					<?php endif;?>
				</div>

		</li>
		<?php endwhile;endif;wp_reset_postdata();?>
	</ul>
</div>
<!-- newsList out -->
<div class="indexUnit01">
	<div class="productsArea">
		<h3 class="ttl"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/index_ttl01.png" alt="国内外のさまざまな分野へ高品質な製品をお届けしています。"></h3>
		<?php if( have_rows('top_product_above','Options') ):?>
		<ul class="photoList01">
			<?php
				while ( have_rows('top_product_above','Options') ) : the_row();
				$top_product_above_img = get_sub_field('top_product_above_img');
				$top_product_above_img_src = wp_get_attachment_image_src($top_product_above_img , 'full');
			?>
				<li>
					<img src="<?php echo $top_product_above_img_src[0]; ?>" alt="<?php echo get_sub_field('top_product_above_img_alt');?>">
				</li>
			<?php endwhile;?>
		</ul>
		<?php endif;?>

		<?php if( have_rows('top_product_bottom','Options') ):?>
		<ul class="photoList02">
			<?php
				while ( have_rows('top_product_bottom','Options') ) : the_row();
				$top_product_bottom_img = get_sub_field('top_product_bottom_img');
				$top_product_bottom_img_src = wp_get_attachment_image_src($top_product_bottom_img , 'full');
			?>
				<li>
					<img src="<?php echo $top_product_bottom_img_src[0]; ?>" alt="<?php echo get_sub_field('top_product_bottom_img_alt');?>">
				</li>
			<?php endwhile;?>
		</ul>
		<?php endif;?>
		<div class="btn">
			<a href="<?php echo home_url('products');?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/index_btn01.png" alt="取扱商品を見る"></a>
		</div>
	</div>
	<!-- productsArea out -->
	<div class="ecoArea">
		<div class="visual"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/index_visual.png" alt="かけがえのない地球のために 2001年6月27日 ISO 14001の認証を取得 環境重視を経営の最重要課題の一つと考え、株式会社サカノシタは環境負荷の少ない商品やサービスのご提供を目指してまいります。">
		</div>
		<div class="btn">
			<a href="<?php echo home_url('eco');?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/index_btn02.png" alt="環境への取り組み"></a>
		</div>
	</div>
	<!-- ecoArea out -->
</div>
<!-- indexUnit01 out -->
<div class="indexUnit02">
	<div class="recruitArea">
		<h3 class="ttl"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/index_ttl02.png" alt="2018年度新卒採用のエントリー受付中">
</h3>
		<div class="entryBtn">
			<a href="https://www.gakujo.ne.jp/2018/company/cmp_baseinfo.aspx?p1=5136" onClick="ga('send','event','link','click','recruit_top_center');" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/entry_btn.jpg" alt="あさがくナビ"></a>
		</div>
	</div>
</div>
<!-- indexUnit02 -->
<div class="indexUnit03">
	<ul>
		<li>
			<a href="https://www.gakujo.ne.jp/2018/company/cmp_stfheart.aspx?p1=5136" onClick="ga('send','event','link','click','recruit_top_business');" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/index_btn03.jpg" alt="会社の雰囲気＆仕事内容を見る"></a>
		</li>
		<li>
			<a href="https://www.gakujo.ne.jp/2018/company/cmp_naitei.aspx?p1=5136" onClick="ga('send','event','link','click','recruit_top_interview');" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/index_btn04.jpg" alt="社員インタビューを見る"></a>
		</li>
	</ul>
</div>
<!-- indexUnit03 out -->
<div class="indexUnit04">
	<div class="clientArea">
		<h3 class="ttl"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/index_ttl03.png" alt="取引先メーカー様のご紹介"></h3>
		<?php if( have_rows('top_vendor','Options') ):?>
		<ul class="clearfix">
			<?php
				while ( have_rows('top_vendor','Options') ) : the_row();
				$top_vendor_ttl = get_sub_field('top_vendor_ttl');
				$top_vendor_text = get_sub_field('top_vendor_text');
				$top_vendor_img = get_sub_field('top_vendor_img');
				$top_vendor_img_src = wp_get_attachment_image_src($top_vendor_img , 'full');
			?>
				<li>
					<?php if($top_vendor_img):?>
					<div class="photo"><img src="<?php echo $top_vendor_img_src[0]; ?>"></div>
					<?php endif;?>
					<dl>
						<dt><?php echo $top_vendor_ttl;?></dt>
						<dd><?php echo $top_vendor_text;?></dd>
					</dl>
				</li>
			<?php endwhile;?>
		</ul>
		<?php endif;?>

	</div>
</div>
<!-- indexUnit04 out -->
<?php get_footer(); ?>
