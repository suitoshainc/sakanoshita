<?php get_header(); ?>
<?php echo get_field('page_ttl');?>
<?php
if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('
<p class="pagePath clearfix">','</p>
');
}
?>
<?php the_content();?>


<div class="outlineBox">
	<h3 class="ttlLev1">会社概要</h3>
	<div class="innerBox">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableStyle01">
		<tbody>
		<?php
			if( have_rows('about_loop') ):
			while ( have_rows('about_loop') ) : the_row();
		?>
			<tr>
				<th><?php echo get_sub_field('about_title');?></th>
				<td><?php echo get_sub_field('about_content');?></td>
			</tr>
		<?php endwhile;endif;?>

		</tbody></table>
	</div><!-- innerBox out -->
</div>

<div class="outlineBox">
<div class="indexUnit_movie" style="text-align: center;">
	<a href="<?php echo get_stylesheet_directory_uri(); ?>/movie.html" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/banner_movie.png" alt=""> </a>
</div>
</div>

<div class="outlineBox">
<h3 class="ttlLev1">事業所</h3>
<div class="innerBox">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableStyle01">
	<caption>国内</caption>
	<tbody>
		<?php
			$myQuery = new WP_Query();
			$param = array(
				'posts_per_page' => -1,
				'post_type' => 'company1'
			);
			$myQuery->query($param);
		?>
		<?php if($myQuery->have_posts()): while($myQuery->have_posts()) : $myQuery->the_post(); ?>
			<tr>
				<th><?php the_title();?></th>
				<td class="txtArea"><?php echo get_field('map_list_text');?></td>
				<td class="btnArea">
					<?php if(get_field('map_list_html')):?>
						<a href="javascript:void(0);" onclick="window.open('<?php the_permalink();?>', '<?php the_title();?>', 'width=500,height=600,top=' + (screen.availHeight-600)/2 + ',left=' + (screen.availWidth-1200)/2);"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/company_btn01.png" width="110" height="40" alt="">
					</a>
					<?php endif;?>
				</td>
			</tr>
		<?php endwhile;endif;wp_reset_postdata();?>
	</tbody>
</table>
</div><!-- innerBox out -->
<br>
<div class="innerBox">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableStyle01">
		<caption>海外</caption>
		<tbody>
			<?php
				$myQuery = new WP_Query();
				$param = array(
					'posts_per_page' => -1,
					'post_type' => 'company2'
				);
				$myQuery->query($param);
			?>
			<?php if($myQuery->have_posts()): while($myQuery->have_posts()) : $myQuery->the_post(); ?>
				<tr>
					<th><?php the_title();?></th>
					<td class="txtArea"><?php echo get_field('map_list_text');?></td>
					<td class="btnArea">
						<?php if(get_field('map_list_html')):?>
							<a href="javascript:void(0);" onclick="window.open('<?php the_permalink();?>', '<?php the_title();?>', 'width=500,height=600,top=' + (screen.availHeight-600)/2 + ',left=' + (screen.availWidth-1200)/2);"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/company_btn01.png" width="110" height="40" alt="">
						</a>
						<?php endif;?>
					</td>
				</tr>
			<?php endwhile;endif;wp_reset_postdata();?>
		</tbody>
	</table>
</div><!-- innerBox out -->
<br>
<div class="innerBox">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableStyle01">
		<caption>関連会社</caption>
		<tbody>
			<?php
				$myQuery = new WP_Query();
				$param = array(
					'posts_per_page' => -1,
					'post_type' => 'company3'
				);
				$myQuery->query($param);
			?>
			<?php if($myQuery->have_posts()): while($myQuery->have_posts()) : $myQuery->the_post(); ?>
				<tr>
					<th><?php the_title();?></th>
					<td class="txtArea"><?php echo get_field('relate_c_text');?></td>
					<td class="btnArea">
						<?php if(get_field('relae_c_link')):?>
							<a href="<?php echo get_field('relae_c_link');?>" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/company_btn02.png" width="110" height="40" alt=""></a>
						<?php endif;?>
					</td>
				</tr>
			<?php endwhile;endif;wp_reset_postdata();?>
		</tbody>
	</table>
</div><!-- innerBox out -->

</div>


<div class="outlineBox">
	<h3 class="ttlLev1">沿革</h3>
	<div class="innerBox">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableStyle02">
			<tbody>
				<?php
					if( have_rows('histories') ):
					while ( have_rows('histories') ) : the_row();
				?>
					<tr>
						<th><?php echo get_sub_field('h_title');?></th>
						<td><?php echo get_sub_field('h_content');?></td>
					</tr>
				<?php endwhile;endif;?>
			</tbody>
		</table>
	</div><!-- innerBox out -->
</div>

<?php get_footer(); ?>
