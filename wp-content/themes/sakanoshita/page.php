<?php get_header(); ?>
<?php echo get_field('page_ttl');?>
<?php
if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('
<p class="pagePath clearfix">','</p>
');
}
?>
<?php the_content();?>
<?php get_footer(); ?>
