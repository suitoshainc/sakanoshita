<?php
// metaやlinkの削除
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'parent_post_rel_link', '10');
remove_action('wp_head', 'start_post_rel_link', '10');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');
remove_action( 'wp_head','wp_shortlink_wp_head',10, 0 );
remove_action('wp_head', 'feed_links_extra',3,0);

// headに含まれるインラインスタイルの削除
function remove_recent_comments_style() {
	global $wp_widget_factory;
	remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
}
add_action( 'widgets_init', 'remove_recent_comments_style' );

//コンテンツフィルターからpタグの削除
remove_filter ('the_content', 'wpautop');
//抜粋からpタグの削除
remove_filter('the_excerpt', 'wpautop');

// //オリジナルの画像サイズを追加
add_image_size( 'photo195-9999', 195, 9999, true );

/* ===============================================
css,js読み込み
=============================================== */
function suitosha_scripts(){
	wp_enqueue_style( 'reset-css', get_stylesheet_directory_uri().'/css/reset.css');
	wp_enqueue_style( 'base-css', get_stylesheet_directory_uri().'/css/base.css');
	wp_enqueue_style( 'mobile-css', get_stylesheet_directory_uri().'/css/mobile.css');
	wp_enqueue_style( 'bxslider-css', get_stylesheet_directory_uri().'/js/jquery.bxslider/jquery.bxslider.css');

	wp_enqueue_script('jquery');
	wp_enqueue_script('bxslider-js', get_stylesheet_directory_uri().'/js/jquery.bxslider/jquery.bxslider.js',array('jquery'));
	wp_enqueue_script('common-js', get_stylesheet_directory_uri().'/js/common.js',array('jquery'));
	wp_enqueue_script('matchHeight-js', get_stylesheet_directory_uri().'/js/jquery.matchHeight.js',array('jquery'));

}
add_action( 'wp_enqueue_scripts', 'suitosha_scripts' );
?>
