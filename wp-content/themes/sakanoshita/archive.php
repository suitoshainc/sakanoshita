<?php get_header(); ?>
	<?php
	global $wp_query;
	global $type_and_tax;

	$target = array('products_auto','products_machine','products_handling');
	$post_type = get_query_var('post_type');

	if(in_array($post_type,$target)):
		include( locate_template('modules/product_list.php', false, false ) );
	elseif($post_type == 'example'):
		include( locate_template('modules/example_list.php', false, false ) );
	elseif($post_type == 'news'):
		include( locate_template('modules/news_list.php', false, false ) );
	?>
	<?php endif;?>
<?php get_footer(); ?>
