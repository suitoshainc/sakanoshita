<?php
global $wp_query;
global $type_and_tax;

$target = array('products_auto','products_machine','products_handling');
$target2 = array('company2','company1');
$post_type = get_query_var('post_type');

if(in_array($post_type,$target)):
	get_template_part('modules/popup');
elseif(in_array($post_type,$target2)):
	get_template_part('modules/map');
else:
?>
<?php get_header(); ?>
	<?php the_content();?>
<?php get_footer(); ?>
<?php endif;?>
