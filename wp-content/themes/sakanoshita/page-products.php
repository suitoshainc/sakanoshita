<?php get_header(); ?>
<div class="products">
	<?php echo get_field('page_ttl');?>
	<?php
	if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('
	<p class="pagePath clearfix">','</p>
	');
	}
	?>
	<div class="outlineBox">
		<?php the_content();?>
	</div>

	<?php if( have_rows('category_data','Options') ):?>
		<?php
		while ( have_rows('category_data','Options') ) : the_row();
		$cat_img = get_sub_field('cat_img');
		$cat_img_src = wp_get_attachment_image_src($cat_img , 'full');
		$col = get_sub_field('cat_list');

		$target = get_sub_field('cat_target');
		$terms = get_terms($target,'orderby=order&hide_empty=0');

		$terms_chunked = array_chunk($terms, $col);
		$class = 'goodsList'.$col.'Col';

		$post_type = str_replace( '_category', '', $target );
		?>

		<div class="outlineBox">
			<h4 class="ttlLev2"><?php echo get_sub_field('cat_ttl');?></h4>
			<div class="innerBox">
				<h5 class="ttlLev3"><?php echo get_sub_field('cat_lead');?></h5>
				<div class="products2Col clearfix mb1em">
				<div class="photo"><img src="<?php echo $cat_img_src[0];?>" width="177" height="177"></div>
				<p class="txt"><?php echo get_sub_field('cat_desc');?></p>
				</div>
				<?php foreach($terms_chunked as $tc):?>
				<ul class="<?php echo $class;?> clearfix">
					<?php foreach($tc as $t):?>
						<li><a href="<?php echo home_url($post_type);?>#<?php echo $t->slug;?>"><?php echo $t->name;?></a></li>
					<?php endforeach;?>
				</ul>
				<?php endforeach;?>
			</div><!-- innerBox out -->
		</div>

		<?php endwhile;?>
	<?php endif;?>
</div>
<?php get_footer(); ?>
