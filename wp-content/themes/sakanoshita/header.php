<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width">
	<meta name="format-detection" content="telephone=no">
	<meta name="description" content="輸入切削工具・輸入空気圧機器・機械工具・自動化関連機器販売－株式会社サカノシタ">
	<title>株式会社サカノシタ</title>
	<?php wp_head();?>
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/css3-mediaqueries.js"></script>
	<![endif]-->
	<script>
	(function(i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function() {
			(i[r].q = i[r].q || []).push(arguments)
		}, i[r].l = 1 * new Date();
		a = s.createElement(o),
			m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
	ga('create', 'UA-92762825-1', 'auto');
	ga('require', 'displayfeatures');
	ga('send', 'pageview');
	</script>
</head>

<body <?php echo body_class();?>>
	<div id="top" class="container">
		<header>
			<div class="header mobNone">
				<div class="unitFirst clearfix">
					<p class="description">輸入切削工具・輸入空気圧機器・機械工具・自動化関連機器販売－株式会社サカノシタ</p>
					<ul class="headerNavi clearfix">
						<li><a href="<?php echo home_url();?>">HOME</a></li>
						<li><a href="<?php echo home_url('recruit');?>">採用情報</a></li>
						<li><a href="<?php echo home_url('contact');?>">お問合せ･資料請求</a></li>
					</ul>
					<!-- headerNavi out -->
				</div>
				<!-- unitFirst out -->
				<div class="unitLast clearfix">
					<h1 class="logo"><a href="<?php echo home_url();?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" width="262" height="62" alt="株式会社サカノシタ Sakanoshita machine＆electric"></a></h1>
					<ul class="navi clearfix">
						<li class="company">
							<a href="<?php echo home_url('company')?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/navi01.png" width="131" height="62" alt="会社案内 company"></a>
						</li>
						<li class="eco">
							<a href="<?php echo home_url('eco')?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/navi02.png" width="158" height="62" alt="環境への取組 eco"></a>
						</li>
						<li class="products">
							<a href="<?php echo home_url('products')?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/navi03.png" width="130" height="62" alt="取扱商品 products"></a>
						</li>
						<li class="example">
							<a href="<?php echo home_url('example')?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/navi04.png" width="177" height="62" alt="FA機器導入事例 example"></a>
						</li>
					</ul>
				</div>
				<!-- unitLast out -->
			</div>
			<!-- header out -->
			<div class="header pcNone">
				<div class="menuUnit">
					<div class="toggle">&nbsp;</div>
					<ul class="menu">
						<?php get_template_part('modules/gnavi')?>
					</ul>
				</div>
				<h1 class="logo"><a href="<?php echo home_url();?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="株式会社サカノシタ Sakanoshita machine＆electric"></a></h1>
			</div>
			<!-- header out -->
		</header>
		<main>
			<div class="contents">
