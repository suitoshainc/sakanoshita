</div>
			<!-- contents out -->
		</main>
		<footer>
			<div class="footer">
				<h2 class="logo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/footer_logo.png" width="204" height="60" alt="株式会社サカノシタ Sakanoshita machine＆electric">
</h2>
				<ul class="sns">
					<li class="instagram">
						<a href="https://www.instagram.com/sakanoshita0101/" onClick="ga('send','event','link','click','recruit_top_Instagram');" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/insta_logo.jpg" width="300" height="60" alt="instagram"></a>
					</li>
					<li class="twitter">
						<a href="https://twitter.com/sakanoshita2018" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/twitter_icon.png" width="42" height="36" alt="twitter"></a>
					</li>
				</ul>
				<ul class="footerNavi">
					<?php get_template_part('modules/gnavi')?>
				</ul>
				<div class="pagetop clearfix">
					<a href="#top"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pagetop.jpg" width="52" height="52"></a>
				</div>
				<p class="copyright">&#169; 2017 SAKANOSHITA CO., LTD.</p>
			</div>
			<!-- footer out -->
		</footer>
	</div>
	<!-- container out -->
	<?php wp_footer();?>
</body>

</html>
