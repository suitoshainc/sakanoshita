// JavaScript Document
jQuery.noConflict();
(function($) {

// 途中から表示される「このページのトップへ」ボタン
$(function() {
	var pageTop = $('.pagetop a');
	pageTop.hide();
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
			pageTop.fadeIn();
		} else {
			pageTop.fadeOut();
		}
	});
	pageTop.click(function () {
		$('body, html').animate({scrollTop:0}, 500, 'swing');
		return false;
	});
});
$(function(){
	$('.clientArea ul li').matchHeight();
});

$(function(){
	$('.menu').css('display','none');
	$('.toggle').on('click', function() {
	$('.menu').slideToggle();
	});
});

$(function(){
	$('.newsList a[href*="http"]').not('[href*="'+location.hostname+'"]').not('.no_icon').not('.not_blank').attr({target:"_blank"}).addClass('blank');

	var t = ":not(.no_icon):not(:has(img)):not(#bread a)";
	$(".newsList").find(t + "[href$='.doc']").addClass("icon_doc file_link").attr("target", "_blank");

	$(".newsList").find(t + "[href$='.xls']").addClass("icon_xls file_link").attr("target", "_blank").end()
	.find(t + "[href$='.docx']").addClass("icon_doc file_link").attr("target", "_blank").end()
	.find(t + "[href$='.doc']").addClass("icon_doc file_link").attr("target", "_blank").end()
	.find(t + "[href$='.xlsx']").addClass("icon_xls file_link").attr("target", "_blank").end()
	.find(t + "[href$='.pdf']").addClass("icon_pdf file_link").attr("target", "_blank").end()
	.find(t + "[href$='.txt']").addClass("icon_txt file_link").attr("target", "_blank").end();
});


})(jQuery);
